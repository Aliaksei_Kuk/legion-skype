if (window.event === undefined) {
	window.event = {srcElement : undefined};
}

var mainDiv = document.getElementById("main");
var contactDiv = document.getElementById("contactBox");
var messageDiv = document.getElementById("messageBox");
var separatorDiv = document.getElementById("separatorNigga");

ResizeManager.setLimit(mainDiv.offsetWidth * 0.25, mainDiv.offsetWidth * 0.45);

EventsManager.add(separatorDiv, "mousedown", ResizeManager.resizingOn);
EventsManager.add(mainDiv, "mouseup", ResizeManager.resizingOff);
EventsManager.add(mainDiv, "mousemove", ResizeManager.resizingProc);

console.log("resize status : ON");

MessageManager.setContainElement(document.getElementById("outputArea"));

var contactsDiv = document.getElementById('contactArea');

var ContactsManager = new ContactsManager(contactsDiv);
ContactsManager.showContacts();

console.log("contacts list : init");

var searchDiv = document.getElementById("searchArea");
var searchInput = document.getElementById("searchInput");

var findReq = function() {
	ContactsManager.findContacts(searchInput.value);
	return;
};

EventsManager.add(searchInput, "keyup", findReq);
EventsManager.add(searchInput, "change", findReq);

console.log("find status : ON");

var BtnClearManager = new BtnClearManager(searchDiv);

var clearBtnControl = function() {
	if (searchInput.value == "") {
		searchInput.className = "searchInputClear";
		BtnClearManager.hide();
	} else {
		searchInput.className = "searchInputDirty";
		BtnClearManager.show();
	}

	return;
};

EventsManager.add(searchInput, "keyup", clearBtnControl);
EventsManager.add(searchInput, "change", clearBtnControl);

var clearInput = function() {
	searchInput.value = "";
	clearBtnControl();
	EventsManager.dispatch(searchInput, "change");
	return;
};

EventsManager.add(BtnClearManager.btn, "click", clearInput);

console.log("btn clear status : ON");

var inputText = document.getElementById("inputText");
var sendButton = document.getElementById("sendButton");

EventsManager.add(sendButton, "click", function() {
	if (inputText.value.replace(/\s/g, '') == "") {
		inputText.value = "";
		return;
	}

	MessageManager.addMessage(inputText.value, ContactsManager.myContact(),
			ContactsManager.selectedContact());
	inputText.value = "";
});

console.log("initialization finished!");
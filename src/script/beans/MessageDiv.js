function MessageDiv(messageText, userName, date) {
	userName = userName || "";
	date = date || "";
	messageText = messageText || "";

	this.containDiv = document.createElement('div');
	this.headDiv = document.createElement('div');
	this.contentDiv = document.createElement('div');

	this.headDiv.innerHTML = "<b>" + userName + "</b><span class = 'headDate'>"
			+ date.format("yyyy-MM-dd h:mm:ss") + "</span>";

	this.contentDiv.innerText = messageText;

	this.containDiv.appendChild(this.headDiv);
	this.containDiv.appendChild(this.contentDiv);

	MessageDiv.prototype.setMessage = function(messageText) {
		messageText = messageText || "";

		this.contentDiv.innerText = messageText;
		return;
	};

	MessageDiv.prototype.setMsgIn = function() {
		this.contentDiv.className = "otherMessage";
		this.containDiv.className = "otherContainer";
		return;
	};

	MessageDiv.prototype.setMsgOut = function() {
		this.contentDiv.className = "myMessage";
		this.containDiv.className = "myContainer";
		return;
	};

	MessageDiv.prototype.setHeader = function(userName, date) {
		date = date || "";
		messageText = messageText || "";

		this.headDiv.innerHTML = "<b>" + userName
				+ "</b><span class = 'headDate'>"
				+ date.format("yyyy-MM-dd h:mm:ss") + "</span>";
		return;
	};

	MessageDiv.prototype.getDiv = function() {
		return this.containDiv;
	};
};
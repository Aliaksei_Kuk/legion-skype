function BtnClearManager(containElement){
	var btnClear = null;	
	
	(function initialization() {
		btnClear = document.createElement('button');
		btnClear.id = "btnClear";	
		
		return;
	}());
		
	var show = function() {
		if (btnClear.parentNode != containElement) {
			containElement.appendChild(btnClear);		
		}

		console.log('button show');
		return;
	};
	
	var hide = function() {
		if (btnClear.parentNode == containElement) {
			containElement.removeChild(btnClear);		
		}		

		console.log('button hide');
		return;
	};
	

	console.log("create object BtnClearController");	
	return {
		btn : btnClear,
		show : show, 
		hide : hide
	};
};

console.log("initialization class BtnClearController");	
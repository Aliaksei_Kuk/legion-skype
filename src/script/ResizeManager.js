var ResizeManager = (function() {
	var minSizeLeft = 100;
	var maxSizeLeft = 200;

	var resizing = false;

	var setLimit = function(min, max) {
		minSizeLeft = min;
		maxSizeLeft = max;
		return;
	};

	var resizingOn = function() {
		resizing = true;
		console.log("resizing On");
		return;
	};

	var resizingOff = function() {
		resizing = false;
		console.log("resizing Off");
		return;
	};

	var resizingProc = function() {
		if (resizing) {

			var pageX = window.event.clientX + document.body.scrollLeft
					+ document.documentElement.scrollLeft - mainDiv.offsetLeft;

			if (pageX < minSizeLeft) {
				pageX = minSizeLeft;
			}

			if (pageX > maxSizeLeft) {
				pageX = maxSizeLeft;
			}

			contactDiv.style.width = pageX + 'px';
			messageDiv.style.width = (mainDiv.offsetWidth
					- separatorDiv.offsetWidth - pageX - 2)
					+ 'px';
		}

		return;
	};

	return {
		setLimit 		: setLimit,
		resizingOn 		: resizingOn,
		resizingOff 	: resizingOff,
		resizingProc 	: resizingProc
	};
}());

console.log("create and initialization ResizeManager");
var MessageManager = (function() {
	var containElement = null;
	var myId = 0;

	var listDivs = new Array();

	var setContainElement = function(containElement1) {
		containElement = containElement1;
	};

	var showHistory = function(contactId) {
		if (containElement == null) {
			console.log("MessageManager error! containElement not init!");
			return;
		}

		var listMessages = MessageDAO.getListMessages(myId, contactId);
		var messagesCount = listMessages.length;

		var parentContainElem = containElement.parentNode;
		parentContainElem.removeChild(containElement);

		var countAvailbleDivs = Math.min(listDivs.length, messagesCount);

		for ( var i = 0; i < countAvailbleDivs; i++) {
			listDivs[i].setMessage(listMessages[i].message);			
			listDivs[i].setHeader("", listMessages[i].date);			
			
			if (listMessages[i].fromId == myId) {
				listDivs[i].setMsgOut();
			} else {
				listDivs[i].setMsgIn();
			}

			containElement.appendChild(listDivs[i].getDiv());
		}

		for ( var i = countAvailbleDivs; i < messagesCount; i++) {
			var newDiv = new MessageDiv(listMessages[i].message, "", listMessages[i].date);

			if (listMessages[i].fromId == myId) {
				newDiv.setMsgOut();
			} else {
				newDiv.setMsgIn();
			}
			listDivs[i] = newDiv;

			containElement.appendChild(newDiv.getDiv());
		}
		
		parentContainElem.insertBefore(containElement,
				parentContainElem.children[0]);
		
		containElement.scrollTop = containElement.scrollHeight;
		
		return;
	};

	var hideHistory = function() {
		if (containElement == null) {
			console.log("MessageManager error! containElement not init!");
			return;
		}
		var parentContainElem = containElement.parentNode;
		parentContainElem.removeChild(containElement);
		
		while (containElement.hasChildNodes()) {
			containElement.removeChild(containElement.firstChild);
		}

		
		parentContainElem.insertBefore(containElement,
				parentContainElem.children[0]);

		containElement.scrollTop = containElement.scrollHeight;
		
		return;
	};
	
	var addMessage = function(messageText, fromContact, toContact){
		if (messageText == "" || fromContact == null || toContact == null){
			return;
		}
		
		message = new Message(0, fromContact.id, toContact.id, new Date, messageText);
		
		MessageDAO.addMessage(message);
		
		hideHistory();
		showHistory(toContact.id);
		
		return;
	};
	
	console.log("create object MessageManager");
	return {
		setContainElement 	: setContainElement,
		showHistory 		: showHistory,
		hideHistory 		: hideHistory,
		addMessage			: addMessage
	};
}());

console.log("initialization class MessageManager");
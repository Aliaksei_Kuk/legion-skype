function ContactsManager(containElement) {

	var listContacts = ContactDAO.getListContacts();
	var listContactDiv = [];

	var oldFindString = "";
	
	var countContacts = listContacts.length;

	var selectedDiv = undefined;
	var selectedContact = null;
	var myContact = ContactDAO.getContactById(0);
	
	(function createDivs() {
		var newDiv;
		for ( var i = 0; i < countContacts; i++) {
			newDiv = document.createElement('div');
			newDiv.id = "contact" + i;

			contact = listContacts[i];

			newDiv.contact = contact;
			
			newDiv.innerHTML = contact.nick;
			newDiv.className = "unselectedContact";
			
			EventsManager.add(newDiv, "click", selectContact);

			listContactDiv[i] = newDiv;
			listContactDiv[i].display = true;
		}

		console.log('create contacts divs');
		return;
	}());

	function selectContact() {
		var elem = event.srcElement || this;

		if (selectedDiv !== undefined) {
			if (selectedDiv === elem) {
				if (elem.className == "unselectedContact") {
					elem.className = "selectedContact";
					selectedContact = elem.contact;
					MessageManager.showHistory(elem.contact.id);
				} else {
					elem.className = "unselectedContact";
					selectedContact = null;
					MessageManager.hideHistory();
				}
				console.log("element " + elem.id + " class: " + elem.className);
				return;
			}

			selectedDiv.className = "unselectedContact";
			selectedContact = null;
			MessageManager.hideHistory();
		}

		selectedDiv = elem;
		elem.className = "selectedContact";
		selectedContact = elem.contact;
		MessageManager.showHistory(elem.contact.id);

		console.log("element " + elem.id + " class: " + elem.className);
		return;
	}

	var showContacts = function() {

		var parentContainElem = containElement.parentNode;
		parentContainElem.removeChild(containElement);

		for ( var i = 0; i < countContacts; i++) {
			containElement.appendChild(listContactDiv[i]);
		}

		parentContainElem.appendChild(containElement);

		console.log('show contacts list');
		return;
	};

	var shielding = function(text) {
		return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
	};
	
	var findContacts = function(findString) {
		
		if (oldFindString == findString) {
			return;
		} else {
			oldFindString = findString;
		}		
		
		var parentContainElem = containElement.parentNode;
		parentContainElem.removeChild(containElement);

		var reg = new RegExp("(.*)(" + shielding(findString) + ")(.*)", "i");
		var matches;

		for ( var i = 0; i < countContacts; i++) {
			var tempDiv = listContactDiv[i];
			
			if (tempDiv.display) {
				containElement.removeChild(tempDiv);
				tempDiv.display = false;
			}

			if ((matches = (listContacts[i].nick).match(reg)) != null) {					
				containElement.appendChild(tempDiv);
				tempDiv.display = true;
				
				tempDiv.innerHTML = matches[1] + "<span class='foundSymbols'>"
						+ matches[2] + "</span>" + matches[3];
			}

		}

		parentContainElem.appendChild(containElement);

		console.log('find contacts');
		return;
	};

	console.log("create object ContactsManager");	
	return {
		showContacts 	: showContacts,
		findContacts 	: findContacts,
		selectedContact : function(){return selectedContact;},
		myContact		: function(){return myContact;}
	};
};

console.log("initialization class ContactsManager");	
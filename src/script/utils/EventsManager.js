var EventsManager = (function() {

	var add;
	if (window.addEventListener != undefined) {
		add = function(elem, type, handler) {
			elem.addEventListener(type, handler, false);
			console.log("attach event: type:" + type + "; element: " + elem.id
					+ ";");
		};
	} else {
		add = function(elem, type, handler) {
			elem.attachEvent("on" + type, handler);
			console.log("attach event: type:" + type + "; element: " + elem.id
					+ ";");
		};
	}

	var remove;
	if (window.removeEventListener != undefined) {
		remove = function(elem, type, handler) {
			elem.removeEventListener(type, handler, false);
			console.log("detach event: type:" + type + "; element: " + elem.id
					+ ";");
		};
	} else {
		remove = function(elem, type, handler) {
			elem.detachEvent("on" + type, handler);
			console.log("detach event: type:" + type + "; element: " + elem.id
					+ ";");
		};
	}

	var dispatch = function(elem, type) {
		var event;
		if (document.createEvent) {
			event = document.createEvent("HTMLEvents");
			event.initEvent(type, true, true);
		} else {
			event = document.createEventObject();
			event.eventType = type;
		}

		event.eventName = "";
		event.memo = {};

		if (document.createEvent) {
			elem.dispatchEvent(event);
		} else {
			elem.fireEvent("on" + event.eventType, event);
		}
		
		console.log("dispatch event: type:" + type + "; element: " + elem.id
				+ ";");
	};

	return {
		add 		: add,
		remove 		: remove,
		dispatch 	: dispatch
	};
}());

console.log("create and initializate EventsManager");
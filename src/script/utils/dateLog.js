if (window.console != undefined && window.console.log != undefined && window.console.log.bind != undefined) {
	console.logCopy = console.log.bind(console);

	console.log = function(data) {
		var currentDate = '[' + new Date().toUTCString() + '] ';
		this.logCopy(currentDate, data);
	};
}

if (window.console == undefined || window.console.log == undefined) {
	window.console = {};
	window.console.log = function(){return;};
}
console.log("creating MessageDAO : starting");

var MessageDAO = (function() {
	var listMessages = [
			new Message(0, 0, 1, new Date(2013, 0, 1, 0, 0, 1),
					'Happy New Year!'),
			new Message(1, 0, 2, new Date(2013, 0, 1, 0, 0, 2),
					'Happy New Year!'),
			new Message(2, 0, 3, new Date(2013, 0, 1, 0, 0, 3),
					'Happy New Year!'),
			new Message(3, 0, 4, new Date(2013, 0, 1, 0, 0, 4),
					'Happy New Year!'),
			new Message(4, 1, 0, new Date(2013, 0, 1, 0, 0, 4), 'Thanks, bro!'),
			new Message(5, 2, 0, new Date(2013, 0, 1, 0, 0, 4),
					'Oh, moroz morooooz! Please stop it, I am coooold!'),
			new Message(6, 3, 0, new Date(2013, 0, 1, 0, 0, 4),
					'Dude, we\'re on a drinking binge! LOL.'),
			new Message(7, 4, 0, new Date(2013, 0, 1, 0, 0, 4),
					'Happy New Year, too!'),
			new Message(8, 0, 5, new Date(2013, 0, 1, 0, 0, 5),
					'Happy New Year!'),
			new Message(9, 5, 0, new Date(2013, 0, 1, 0, 0, 6),
					'Happy New Year!'),
			new Message(10, 5, 0, new Date(2013, 0, 1, 0, 0, 7),
					'bla bla bla bla bla bla bla bla bla bla bla '
							+ 'bla bla bla bla bla bla bla bla bla bla bla '
							+ 'bla bla bla bla bla bla bla bla bla bla bla '
							+ 'bla bla bla bla bla bla bla bla bla bla bla '
							+ 'bla bla bla bla bla bla bla bla bla bla bla '
							+ 'bla bla bla bla bla bla bla bla bla bla bla '
							+ 'bla bla bla bla bla bla bla bla bla bla bla '
							+ 'bla bla bla bla bla bla bla bla bla bla bla '
							+ 'bla bla bla bla bla bla bla bla bla bla bla '
							+ 'bla bla bla bla bla bla bla bla bla bla bla '
							+ 'bla bla bla bla bla bla bla bla bla bla bla '),
			new Message(11, 0, 5, new Date(2013, 0, 1, 0, 0, 8), 'ok'),
			new Message(12, 5, 0, new Date(2013, 0, 1, 0, 0, 9), 'and '
					+ 'bla bla bla bla bla bla bla bla bla bla bla '
					+ 'bla bla bla bla bla bla bla bla bla bla bla '
					+ 'bla bla bla bla bla bla bla bla bla bla bla '
					+ 'bla bla bla bla bla bla bla bla bla bla bla '
					+ 'bla bla bla bla bla bla bla bla bla bla bla '
					+ 'bla bla bla bla bla bla bla bla bla bla bla '
					+ 'bla bla bla bla bla bla bla bla bla bla bla '
					+ 'bla bla bla bla bla bla bla bla bla bla bla '
					+ 'bla bla bla bla bla bla bla bla bla bla bla '
					+ 'bla bla bla bla bla bla bla bla bla bla bla '
					+ 'bla bla bla bla bla bla bla bla bla bla bla '
					+ 'bla bla bla bla bla bla bla bla bla bla bla '
					+ 'bla bla bla bla bla bla bla bla bla bla bla '
					+ 'bla bla bla bla bla bla bla bla bla bla bla '
					+ 'bla bla bla bla bla bla bla bla bla bla bla '
					+ 'bla bla bla bla bla bla bla bla bla bla bla '
					+ 'bla bla bla bla bla bla bla bla bla bla bla '
					+ 'bla bla bla bla bla bla bla bla bla bla bla '
					+ 'bla bla bla bla bla bla bla bla bla bla bla ') ];

	var lastFreeId = 13;
	
	var getListMessages = function(contact1Id, contact2Id) {
		var resultListMessages = new Array();

		var j = 0;

		for ( var i = 0; i < listMessages.length; i++) {
			var message = listMessages[i];
			if ((message.fromId === contact1Id && message.toId === contact2Id)
					|| (message.fromId === contact2Id && message.toId === contact1Id)) {
				resultListMessages[j++] = message;
			}
		}

		return resultListMessages;
	};

	var addMessage = function(message) {
		message.id = lastFreeId++;
		listMessages[listMessages.length] = message;
		return true;
	};
	
	return {
		getListMessages : getListMessages,
		addMessage : addMessage
	};
}());

console.log("creating MessageDAO");
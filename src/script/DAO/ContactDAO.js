console.log("creating ContactDAO : starting");

var ContactDAO = (function() {
	var listContacts = [ new Contact(0, "Maximus", "max123"),
			new Contact(1, "Minimus", "min123"),
			new Contact(2, "Medius", "med123"),
			new Contact(3, "Moreus", "mor123"),
			new Contact(4, "Mechanicus", "mec123"),
			new Contact(5, "Astropaticum", "ast123") ];	
	return {
		getListContacts : function() {
			var resultListContacts = new Array();
			
			for (var i = 1; i < listContacts.length; i++) {
				resultListContacts[i - 1] = listContacts[i];
			}
			
			return resultListContacts;
		}, 
		getContactById : function(id) {			
			return listContacts[id];
		}
	};
}());

console.log("create ContactDAO");